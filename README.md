# CarCar -- Car Dealership Management Software


#### Software Engineering Team:
- Kyle - Sales microservice
- Louis - Service microservice


## Built With:
- Python (Django)
- Javascript
- React
- Bootstrap
- Docker
- PostgreSQL
- Insomnia
- HTML
- CSS


## Design

The page will be styled using bootstrap, but some elements will be styled using traditional css. The navigation bar will have a dropdown menu for each of the bounded contexts and each will contain links to the appropriate pages. We've added function and search features to sort through each of the tables based on the data that each column represents.

## Service microservice

My microservice will have separate models for technicians and appointments. The Appointments model will have a foreign key to the Technician model to grab the technician working on the case. Appointments will also have data relating to the customer, time of the appointment, the reason for the visit, and the VIN.

Additionally, I will be pulling automobile data from the Inventory microservice, specifically the vin number which will be used to match up with appointment requests. When an appointment is made I will use this data to determine whether the customer is a VIP. This data will be obtained from the inventory microservice through use of a poller to an Automobile value object.

Technician and appointment data will be collected via form submissions and available in the Appointments List and appointments history tabs. There the data can be manipulated, searched and sorted based on the needs of the operating company.

## API Reference

```http://localhost:8080

  GET /api/appointments/
  GET /appointments/<int:id>/
  POST /api/appointments/
  GET /appointments/<int:id>/
  DELETE /appointments/<int:id>/
  GET /api/technician/
  POST /api/technician/
  DELETE /technician/<int:id>/
  GET /api/automobiles/
```

## Sales microservice

I will be creating models for the salesperson, customer, and sales record. The sales record will contain foreing keys to both a customer and salesperson object. It will also have a foreign key to the inventory which will be gathered through the poller and stored in a value object.

The value object will contain a unique VIN indentifier used to access it from the database. It will also contain a "sold" boolean field that will be set to true when a user creates a sale record with that value object's VIN. Once the sale record is created the VIN will not be selectable for future sale records.

The front-end will contain a table of sale records. There will also be a page for listing sale records in a table that can be filtered by salesperson. The table will access the data by matching sale records with the salesperson's employee number. Forms will be used to submit entries for the salesperson, customer, and sale record objects. When a user hits the delete button on a sale record, it will delete the value object from the database and the cascading effect will delete the sale record.
When the poller gets automobile data from inventory it will create a new value object with that vin and the default "sold" value of false, so it can be used in future sale records.

## API Reference

```http://localhost:8090

  GET /api/salespersons/
  POST /api/salespersons/
  GET /api/customers/
  POST /api/customers/
  GET /api/salerecords/
  POST /api/salerecords/
  GET /api/automobiles/
  PUT /api/automobiles/<str:vin>/
  DELETE /api/automobiles/<str:vin>/
```

## Installation

1. Fork and Clone the repo (git clone https://gitlab.com/kyle.merckx/project-beta)
2. Run the following docker commands:
    - docker volume creaet beta-data
    - docker compose build
    - docker compose up
