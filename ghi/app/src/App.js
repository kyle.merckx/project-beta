import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesList from './Sales/SalesList';
import SalespeopleList from './Sales/SalespeopleList';
import AddCustomer from './Sales/AddCustomer';
import AddSalesperson from './Sales/AddSalesperson';
import CreateSalesRecord from './Sales/CreateSalesRecord';
import AppointmentsList from './Services/AppointmentsList'
import AppointmentForm from './Services/AppointmentForm';
import TechnicianForm from './Services/TechnicianForm';
import AppointmentHistory from './Services/AppointmentHistory';
import AddAutomobile from './Inventory/AddAutomobile';
import AutomobileList from './Inventory/AutomobileList';
import AddManufacturer from './Inventory/AddManufacturer';
import ManufacturerList from './Inventory/ManufacturerList';
import AddModel from './Inventory/AddModel';
import ModelList from './Inventory/ModelList';
import "./index.css"


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/sales">
            <Route index element={<SalesList />} />
            <Route path="/sales/create/" element={<CreateSalesRecord/>} />
            <Route path="/sales/salespeople/add/" element={<AddSalesperson />} />
            <Route path="/sales/salespeople/" element={<SalespeopleList />} />
            <Route path="/sales/customers/add/" element={<AddCustomer />} />
          </Route>
          <Route path="/services">
            <Route path="/services/appointments/" element={<AppointmentsList />} />
            <Route path="/services/technician/" element={<TechnicianForm />} />
            <Route path="/services/appointments/new/" element={<AppointmentForm />} />
            <Route path="/services/appointments/history/">
              <Route path="/services/appointments/history/" element={<AppointmentHistory />} />
            </Route>
          </Route>
          <Route path="/inventory">
            <Route path="/inventory/automobiles/" element={<AutomobileList />} />
            <Route path="/inventory/automobiles/add/" element={<AddAutomobile />} />
            <Route path="/inventory/manufacturers/" element={<ManufacturerList />} />
            <Route path="/inventory/manufacturers/add/" element={<AddManufacturer />} />
            <Route path="/inventory/models/" element={<ModelList />} />
            <Route path="/inventory/models/add/" element={<AddModel />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
