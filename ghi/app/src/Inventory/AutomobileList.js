import { useState, useEffect } from "react";
import sortTable from "../SortFunctions";
import NavModal from "./AutoNavModal";

export default function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);
  const [sort, setSort] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [auto, setAuto] = useState({});
  const [activeVin, setActiveVin] = useState("");

  const fetchAutomobiles = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  const handleSort = (event) => {
    const num = parseInt(event.target.dataset.select);
    sortTable(num);
    if (sort === num) {
      setSort(false);
      return;
    }
    setSort(num);
  };

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  var getAutoData = async (vin) => {
    const response = await fetch(
      `http://localhost:8100/api/automobiles/${vin}`
    );
    if (response.ok) {
      const data = await response.json();
      setAuto(data);
    }
  };

  function handleModal(e) {
    setActiveVin(e.target.value);
    getAutoData(e.target.value);
    setOpenModal(!openModal);
    setAuto({});
  }

  function handleCloseModal() {
    setOpenModal(false);
    setAuto({});
  }

  return (
    <div className="bg-white bg-opacity-75 rounded-md">
      <div className="col col-sm-auto mt-3">
        <h1 className="text-2xl">Automobiles</h1>
      </div>
      <table className="table table-striped table-sortable" id="myTable">
        <caption />
        <thead>
          <tr>
            <th
              className={sort === 0 ? "asc" : "desc"}
              data-select="0"
              onClick={handleSort}
            >
              VIN
            </th>
            <th
              className={sort === 1 ? "asc" : "desc"}
              data-select="1"
              onClick={handleSort}
            >
              Color
            </th>
            <th
              className={sort === 2 ? "asc" : "desc"}
              data-select="2"
              onClick={handleSort}
            >
              Year
            </th>
            <th
              className={sort === 3 ? "asc" : "desc"}
              data-select="3"
              onClick={handleSort}
            >
              Model
            </th>
            <th
              className={sort === 4 ? "asc" : "desc"}
              data-select="4"
              onClick={handleSort}
            >
              Manufacturer
            </th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((auto) => {
            return (
              <tr key={auto.id}>
                <td>
                  <button
                    type="button"
                    className="btn btn-link"
                    value={auto.vin}
                    onClick={handleModal}
                  >
                    {auto.vin}
                  </button>
                  {openModal ? (
                    <NavModal
                      closeModal={handleCloseModal}
                      auto={auto}
                      activevin={activeVin}
                    />
                  ) : (
                    <></>
                  )}
                </td>
                <td>{auto.color}</td>
                <td>{auto.year}</td>
                <td>{auto.model.name}</td>
                <td>{auto.model.manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
