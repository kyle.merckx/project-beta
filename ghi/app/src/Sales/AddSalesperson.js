import { useState } from "react";

export default function AddSalesperson() {
    const [formData, setFormData] = useState({
        name: "",
        employee_number: ""
    })

    const handleFormSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8090/api/salespersons/";
        const data = JSON.stringify(formData);
        const fetchConfig = {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);

        if (response.ok){
            setFormData({
                name: "",
                employee_number: ""
            });
        }
    }

    const handleFormChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setFormData({
            ...formData,
            [name]: value
        });
    }

    return(
        <div className="backdrop-blur mt-5 mx-auto shadow pb-3 rounded-md" style={{width: 500}}>
            <div className="bg-gradient-to-l from-blue to-gray-dark rounded-t-md">
                <h1 className="text-white text-center text-2xl">Add a salesperson</h1>
            </div>
            <form onSubmit={handleFormSubmit}>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{width: 300}}>
                    <input onChange={handleFormChange} required name="name" value={formData.name} placeholder="Name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-group form-floating col-5 mx-auto mt-3" style={{width: 300}}>
                    <input onChange={handleFormChange} required name="employee_number" value={formData.employee_number} placeholder="Employee number" className="form-control"/>
                    <label htmlFor="employee_number">Employee number</label>
                </div>
                <div className="d-grid form group col-5 mx-auto" style={{width: 300}}>
                    <button className="btn  bg-blue hover:bg-gray btn-block mt-3">Add</button>
                </div>
            </form>
        </div>
    );
}
