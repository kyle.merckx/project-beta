export default function NavModal({ auto, closeModal, appointment, activevin }) {
  return ((auto.vin === appointment.vin) && auto.model) ? (
    <div className="modalBackground">
      <div className="modalContainer">
        <div className="title">
          <h5> Vehicle Details:</h5>
        </div>
        <div className="body">
          <li> Model: {auto.model.name} </li>
          <li> Manufacturer: {auto.model.manufacturer.name} </li>
          <li> Model Year: {auto.year} </li>
          <li> Color: {auto.color} </li>
          <p>
            <img
              src={auto.model.picture_url}
              className="img-fluid"
              width="250"
            />
          </p>
        </div>
        <div className="footer">
          <button className="btn bg-blue btn-block mt-2" onClick={closeModal}>Close</button>
        </div>
      </div>
    </div>
  ) : appointment.vip === false && appointment.vin == activevin ? (
    <div>
      <p>Car not in system.</p>
      <p>
        <button className="btn bg-blue btn-block mt-2" onClick={closeModal}>Close</button>
      </p>
    </div>
  ) : (
    <></>
  );
}
