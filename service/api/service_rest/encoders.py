from common.json import ModelEncoder
from .models import AutomobileVO, Appointments, Technician


class AutoEncoder(ModelEncoder):
    model = AutomobileVO
    properties = {
        "id",
        "import_href",
        "color",
        "year",
        "model",
        "vin",
        "manufacturer",
        "picture_url",
    }


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = {
        "id",
        "name",
        "employee_number",
    }


class AppointmentsEncoder(ModelEncoder):
    model = Appointments
    properties = {
        "id",
        "vin",
        "customer_name",
        "date",
        "time",
        "technician",
        "reason",
        'vip',
        'finished',
    }
    encoders = {
        "technician": TechnicianEncoder(),
    }
