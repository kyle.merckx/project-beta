from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    model = models.CharField(max_length=5000)
    vin = models.CharField(max_length=17, unique=True)
    manufacturer = models.CharField(max_length=100)
    picture_url = models.CharField(max_length=250)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveBigIntegerField(null=True, blank=False)


class Appointments(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    vip = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    reason = models.TextField(max_length=500)
    technician = models.ForeignKey(
        Technician, related_name="appoitment", on_delete=models.PROTECT
    )
